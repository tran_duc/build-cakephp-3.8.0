-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2019 at 12:16 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cakephp3`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `created`, `modified`) VALUES
(1, 'Đức', 'tranvanduc12a4@gmail.com', '0347834575', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Clarar', 'clarar@gmail.com', '0347834xxx', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'JonhWick', 'JonhWick@gmail.com', '0347831234', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Sona', 'Sona@gmail.com', '0347834111', '2019-05-17 16:10:49', '0000-00-00 00:00:00'),
(6, 'Misa', 'Misa@gmail.com', '034783xxxx', '2019-05-17 16:19:30', '0000-00-00 00:00:00'),
(7, '', '', '', '2019-05-17 17:01:43', '2019-05-17 17:01:43'),
(8, 'Laura', 'Laura@gmail.com', '03478341xx', '2019-05-17 17:09:52', '2019-05-17 17:09:52'),
(9, '', '', '', '2019-05-17 17:12:09', '2019-05-17 17:12:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
