<div class="td-container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-body">
                    <?php echo $this->Form->create($user) ?>
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" name="phone" class="form-control" />
                        </div>
                    <div style="float: left;"><button type="submit" class="btn btn-primary">Submit</button></div>
                    <div style="float: right;"><?php echo $this->Html->link('Back',['action'=>'index'],['class'=>'btn btn-success']); ?></div>
                    <?php echo $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>