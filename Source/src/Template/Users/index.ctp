<div class="td-container">
    <div class="row">
        <div class="col-md-6"><h2>Users</h2></div>
        <div class="col-md-6"><div class="td-link-add"><?php echo $this->Html->link('Add',['action'=>'add'],['class'=>'btn btn-primary']) ?></div></div>
    </div>
    <div><?= $this->Flash->render() ?></div>
    <div class="row">
        <table>
            <thead>
                <th>STT</th>
                <th>name</th>
                <th>created</th>
                <th>modified</th>
                <th>Edit</th>
                <th>Del</th>
            </thead>
            <tbody>
                <?php foreach($users as $key => $user): ?>
                <tr>
                    <td><?php echo $key ?></td>
                    <td><?php echo $user->name ?></td>
                    <td><?php echo $user->created ?></td>
                    <td><?php echo $user->modified ?></td>
                    <td><?php echo $this->Html->link('Edit',['action'=>'edit',$user->id],['class'=>'btn btn-warning']); ?></td>
                    <td><?php echo $this->Html->link('Del',['action'=>'delete',$user->id],['class'=>'btn btn-danger']); ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>