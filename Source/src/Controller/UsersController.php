<?php
namespace App\Controller;
class UsersController extends AppController{


    public function index(){
        $this->set('users',$this->Users->find('all'));
    }
    public function add(){
        $user = $this->Users->newEntity();
        if($this->request->is('post')){
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user,$data);
            //$user->created = date('Y-m-d H:i:s');
            if($this->Users->save($user)){
                $this->redirect(['action'=>'index']);
                $this->Flash->success('Success');
            }else{
                $this->redirect(['action'=>'index']);
                $this->Flash->error('Fail');
            }
        }
        $this->set('user', $user);
    }
    public function edit($id){
        $user = $this->Users->get($id);
        $this->set('name', $user->name);
        $this->set('email', $user->email);
        $this->set('phone', $user->phone);
        $this->set('user', $user);
        if($this->request->is('post','put')){
            $data = $this->request->getData();
            $user = $this->Users->patchEntity($user,$data);
            //$user->modified = date('Y-m-d H:i:s');
            if($this->Users->save($user)){
                $this->redirect(['action'=>'index']);
                $this->Flash->success('Success');
            }else{
                $this->redirect(['action'=>'index']);
                $this->Flash->error('Fail');
            }
        }

    }
}